use scratchcards::get_lines;
use scratchcards::parsing::Card;
use scratchcards::FileToParse::Input;

fn process(lines: impl Iterator<Item = String>) -> u32 {
    // id, score, amount
    let mut book: Vec<(u32, u32, u32)> = lines
        .map(|line| {
            let card = line.parse::<Card>().expect("msg");
            (card.id, card.matching(), 1)
        })
        .collect();

    let og_total = book.len();

    for n in 1..og_total {
        let (prev, curr) = book.split_at_mut(n);

        prev.iter().for_each(|(id, score, amount)| {
            // when the card has non-zero score
            // and score + card number >= n
            if *score != 0 && (id + score) >= (n + 1) as u32 {
                println!("gaining {} copy from {}", amount, id);
                curr[0].2 += amount;
            }
        })
    }

    book.into_iter().map(|t| t.2).sum()
}

fn main() -> std::io::Result<()> {
    let lines = get_lines(Input)?;

    let result = process(lines);

    println!("{result}");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use scratchcards::FileToParse::Test;

    #[test]
    fn test_process() {
        let fixture = get_lines(Test(1)).expect("Failed to load test-1.txt");
        assert_eq!(30, process(fixture));
    }
}
