use scratchcards::get_lines;
use scratchcards::parsing::Card;
use scratchcards::FileToParse::Input;

fn process(lines: impl Iterator<Item = String>) -> u32 {
    lines
        .map(|line| line.parse::<Card>().unwrap().point())
        .sum()
}

fn main() -> std::io::Result<()> {
    let lines = get_lines(Input)?;

    let result = process(lines);

    println!("{result}");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use scratchcards::FileToParse::Test;

    #[test]
    fn test_process() {
        let fixture = get_lines(Test(1)).expect("Failed to load test-1.txt");
        assert_eq!(13, process(fixture));
    }
}
